import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-form-detail",
  templateUrl: "./form-detail.component.html",
  styleUrls: ["./form-detail.component.css"]
})
export class FormDetailComponent implements OnInit {
  NumberOfOption =[{
    'Desc':null,
    'quantity':1,
    'Rate':null,
    'Amount':null
  }];
  quantity=1;
  totalSum=0;
  billFromAddress:string;
  billToAddress:string;
  showBillFrom:boolean;
  showBillTo:boolean;
  
  constructor() {
   }

   addMoreLine(){
     this.NumberOfOption.push({
      'Desc':null,
      'quantity':1,
      'Rate':null,
      'Amount':null
    })
   }

   removeInput(detail,indexValue){
     this.NumberOfOption.splice(indexValue, 1);
     var amount=0;
     this.NumberOfOption.map(function(d){
       amount=amount+d.Amount
     })
     this.totalSum=amount
 
   }

   rateChanged(index,detail){
    detail.Amount=detail.Rate*detail.quantity
    var amount=0;
    this.NumberOfOption.map(function(d){
      amount=amount+d.Amount
    })
    this.totalSum=amount

   }
   getFinalReport(){
    if(!this.billToAddress){
      this.showBillTo=true;
    }else{
      this.showBillTo=false;
    }
     if(!this.billFromAddress){
      this.showBillFrom=true;
     }else{
      this.showBillFrom=false;
     }

   }
   date: Date = new Date();
   date1: Date = new Date();
   settings = {
     bigBanner: false,
     timePicker: false,
     format: "dd-MM-yyyy",
     defaultOpen: false
   };
  ngOnInit() {
  }
}
