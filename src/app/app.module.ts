import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';

import { AppComponent } from './app.component';
import { FormDetailComponent } from './form-detail/form-detail.component';
import { FormDetailFooterComponent } from './form-detail-footer/form-detail-footer.component';

@NgModule({
  declarations: [
    AppComponent,
    FormDetailComponent,
    FormDetailFooterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularDateTimePickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
