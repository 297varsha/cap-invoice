import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDetailFooterComponent } from './form-detail-footer.component';

describe('FormDetailFooterComponent', () => {
  let component: FormDetailFooterComponent;
  let fixture: ComponentFixture<FormDetailFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDetailFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDetailFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
